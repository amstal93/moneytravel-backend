const express = require('express');
const app = express();
const ws = require('ws');
const ws_server = new ws.Server({ port: 3001 });
const pg = require('pg');
const pg_pool = new pg.Pool({ connectionString: process.env.POSTGRES_URI });
const pg_client = new pg.Client({ connectionString: process.env.POSTGRES_URI });
const body_parser = require('body-parser');
const fs = require('fs');
const fb_admin = require('firebase-admin');

fb_admin.initializeApp({
  credential: fb_admin.credential.applicationDefault()
});

const get_auth_token = (req, res, next) => {
  if(req.headers.authorization &&
     req.headers.authorization.split(' ')[0] === 'Bearer') {
    req.authToken = req.headers.authorization.split(' ')[1];
  } else {
    req.authToken = null;
  }
  next();
};

const get_user_display_name_by_uid = async (uid) => {
  const u = await fb_admin.auth().getUser(uid);
  return u.displayName;
}

const check_if_authenticated = (req, res, next) => {
  get_auth_token(req, res, async () => {
    try {
      const { authToken } = req;
      const userInfo = await fb_admin.auth().verifyIdToken(authToken);
      req.authId = userInfo.uid;
      const id = await pg_pool.query({
        text: `SELECT api.try_create_user($1) AS id`,
        values: [userInfo.uid]
      });
      await pg_pool.query(`SET SESSION api.user_id=${id.rows[0].id}`);
      return next();
    } catch (e) {
      log(`FB: ${JSON.stringify(e)}`);
      return res
        .status(401)
        .send({ msg: 'You are not authorized to make this request' });
    }
  });
};

const log = function(msg) {
  console.log(msg);
  fs.appendFile("api.log", `${msg}\n`, function(err) {});
}

app.use(body_parser.json({ limit: '30mb' }));
app.use(body_parser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
  log(`REST: ${req.method} ${req.path}`);
  res.on('finish', () => log(`      => ${res.statusCode}`));
  next();
});

app.get('/', function (req, res) {
    res.json({ msg: 'root of the MoneyTravel REST API' });
});

app.get('/bill_profiles/:serial_number?', check_if_authenticated, function (req, res) {
  let query_columns = '*';
  let query_filter = '';
  let query_values = [];
  if (req.params.serial_number) {
    query_filter = `WHERE serial_number = $1`;
    query_values = [req.params.serial_number];
  }
  if (req.query.short)
    query_columns = 'id, serial_number, first_appearence_timestamp, avatar_picture';
  query = {
    text: `
      SELECT ${query_columns} FROM api.banknote_profiles
      ${query_filter}
    `,
    values: query_values
  };
  pg_pool.query(query, async (err, qres) => {
    if (err) {
      log(`DB: ${err}`);
      res.status(400).json({ msg: 'Sorry! Could not process your request :/' });
      return;
    }
    let preres = (req.params.serial_number ? qres.rows[0] || [] : qres.rows);
    if (!req.query.short) {
      if (req.params.serial_number) {
        preres.first_author_name = await get_user_display_name_by_uid(preres.first_author_id);
        preres.checkins = await Promise.all(preres.checkins.map(async (checkin) => {
          checkin.author_name = await get_user_display_name_by_uid(checkin.author_id);
          return checkin;
        }));
      } else {
        preres = await Promise.all(preres.map(async (bill) => {
          bill.first_author_name = await get_user_display_name_by_uid(bill.first_author_id);
          bill.checkins = await Promise.all(bill.checkins.map(async (checkin) => {
            checkin.author_name = await get_user_display_name_by_uid(checkin.author_id);
            return checkin;
          }));
          return bill;
        }));
      }
    }
    res.status(200).json(preres);
  });
});

app.post('/check_in/:serial_number', check_if_authenticated, function (req, res) {
  pg_pool.query({
    text: `
      SELECT api.checkin ($1, $2, $3, $4, $5, $6, $7) AS status
    `,
    values: [
      req.params.serial_number,
      req.body.timestamp,
      req.body.comment,
      req.body.picture,
      req.body.location_latitude,
      req.body.location_longitude,
      req.body.location_text
    ]
  }, (err, qres) => {
    if (err) {
      log(`DB: ${err}`);
      res.status(400).json({ msg: 'Sorry! Could not process your request :/' });
    } else if (!qres.rows[0].status) {
      res.status(400).json({ msg: 'Sorry! Could not check in your bill :/' });
    } else {
      res.status(201).json({ msg: 'Checked in!' });
    }
  });
});

app.get('/bill_locations', check_if_authenticated, function (req, res) {
  pg_pool.query(`
    SELECT * FROM api.banknote_locations
  `, (err, qres) => {
    if (err) {
      log(`DB: ${err}`);
      res.status(400).json({ msg: 'Sorry! Could not process your request :/' });
    } else {
      res.status(200).json(qres.rows);
    }
  });
});

app.listen(3000, () => log('REST API started'));

;(async function() {
  await pg_client.connect();
  await pg_client.query('LISTEN wslistener');
  log('WS API started');
  ws_server.on('connection', function(sock) {
    log(`WS: new connection`);
    pg_client.on('notification', async function(data) {
      if (sock.readyState === ws.OPEN) {
        sock.send(data.payload);
        log(`WS: ${data.payload}`);
      }
    });
    sock.on('close', function(e) {
      log('WS: connection terminated');
    })
  });
})();

